import {Component, OnInit} from '@angular/core';
import {Vegetable} from '../../shared/vegetable.model';
import {ModalController} from '@ionic/angular';
import {DepositModalComponent} from '../deposit-modal/deposit-modal.component';
import {VegetablesService} from '../../shared/vegetables.service';

@Component({
  selector: 'app-vegetables-list',
  templateUrl: './vegetables-list.component.html',
  styleUrls: ['./vegetables-list.component.scss'],
})
export class VegetablesListComponent implements OnInit {
  seasonalVegetables: Vegetable[] = [];
  vegetableView = 'ascending';

  constructor(private modalCtrl: ModalController, public vegetablesServices: VegetablesService) {
  }

  ngOnInit() {
    this.vegetablesServices.sort('ascending'); // Sort by default vegetables
    const vegetables: Vegetable[] = this.vegetablesServices.getVegetables();

    for (const vegetableItem of vegetables) {
      let currentMonth = new Date().getMonth() + 1; // Get the month as a number (1-12)
      const earlySeason = vegetableItem.season[0];
      let endSeason = vegetableItem.season[1];

      // Add 12 months to "endSeason" if the interval is reverse (EX: JANUARY - APRIL -> APRIL - JANUARY)
      // Add 12 months to "currentMonth" to adjust it if necessary
      // tslint:disable-next-line:no-unused-expression
      endSeason < earlySeason && (endSeason += 12, currentMonth < earlySeason && (currentMonth += 12));

      if ((currentMonth >= earlySeason && currentMonth <= endSeason)) {
        const vegetable: Vegetable = this.vegetablesServices.getVegetable(vegetableItem.name);
        this.seasonalVegetables.push(vegetable);
      }
    }
  }

  onUpdateSegment(event: Event) {
    this.vegetablesServices.sort((event.target as HTMLInputElement).value);
  }

  async onVegetableSelected(vegetable: Vegetable) {
    // Send data to the deposit-modal component
    const modal = await this.modalCtrl.create({
      component: DepositModalComponent,
      componentProps: {
        vegetable
      }
    });
    await modal.present();
  }
}
