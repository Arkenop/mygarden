import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Vegetable} from '../../../shared/vegetable.model';

@Component({
  selector: 'app-vegetable-item',
  templateUrl: './vegetable-item.component.html',
  styleUrls: ['./vegetable-item.component.scss'],
})
export class VegetableItemComponent {
  @Input() vegetable: Vegetable;
  @Output() vegetableSelected = new EventEmitter<void>();

  onSelected() {
    this.vegetableSelected.emit();
  }
}
