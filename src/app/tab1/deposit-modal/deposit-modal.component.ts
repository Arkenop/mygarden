import {Component, Input} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {Vegetable} from '../../shared/vegetable.model';

@Component({
  selector: 'app-deposit-modal',
  templateUrl: './deposit-modal.component.html',
  styleUrls: ['./deposit-modal.component.scss'],
})
export class DepositModalComponent {
  @Input() vegetable: Vegetable;
  segmentValue = 'ID'; // Default segment
  seasonString = [];

  constructor(private modalCtrl: ModalController) {
  }

  onChangeVegetable(event: Vegetable) {
    console.log(event);
    this.vegetable = event;
  }

  dismissModal() {
    this.modalCtrl.dismiss().then();
  }
}
