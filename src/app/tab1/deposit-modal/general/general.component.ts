import {Component, DoCheck, EventEmitter, Input, Output} from '@angular/core';
import {AlertController, ModalController} from '@ionic/angular';
import {VegetablesService} from '../../../shared/vegetables.service';
import {Vegetable} from '../../../shared/vegetable.model';


@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss'],
})
export class GeneralComponent implements DoCheck {

  @Input() vegetable: Vegetable;
  @Output() vegetableSelected = new EventEmitter<Vegetable>();

  seasonString = [];

  constructor(
    private modalCtrl: ModalController,
    public alertController: AlertController,
    private vegetableService: VegetablesService
  ) {
  }

  async updateModal(vegetableSelected) {
    const vegetable: Vegetable | null = this.vegetableService.getVegetable(vegetableSelected);

    if (vegetable !== null) {
      this.vegetableSelected.emit(vegetable);
    } else {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Erreur',
        subHeader: 'Légume ou fruit non disponible',
        message: 'Ce légume (fruit) que vous recherché n\'est pas renseigné dans la base de donnée pour le moment.',
        buttons: ['OK']
      });
      await alert.present();
    }
  }

  ngDoCheck() {
    // Convert the month 0-12 to a string Jan-Dec
    for (let i = 0; i < this.vegetable.season.length; i++) {
      const d = this.vegetable.season[i] - 1; // Convert 1-12 to 0-11
      const month = [];
      month[0] = 'Janv';
      month[1] = 'Fév';
      month[2] = 'Mars';
      month[3] = 'Avr';
      month[4] = 'Mai';
      month[5] = 'Juin';
      month[6] = 'Juil';
      month[7] = 'Août';
      month[8] = 'Sep';
      month[9] = 'Oct';
      month[10] = 'Nov';
      month[11] = 'Déc';
      this.seasonString[i] = month[d];
    }
  }
}
