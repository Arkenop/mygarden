/**
 * Vegetable model
 * @author DERACHE Adrien
 */
export class Vegetable {

  public name: string;
  public imagePath: string;
  public synopsis: string;
  public season: number[];
  public family: string;
  public preservation: string;
  public nutritional: string;
  public vitamin: string;
  public history: string;
  public ally: string[];
  public enemy: string[];

  constructor(
    name: string,
    family: string,
    synopsis: string,
    season: number[],
    preservation: string,
    nutritional: string,
    vitamin: string,
    ally: string[],
    enemy: string[],
    history: string,
    imagePath: string
  ) {
    this.name = name;
    this.family = family;
    this.synopsis = synopsis;
    this.season = season;
    this.preservation = preservation;
    this.nutritional = nutritional;
    this.vitamin = vitamin;
    this.ally = ally;
    this.enemy = enemy;
    this.history = history;
    this.imagePath = imagePath;
  }
}
