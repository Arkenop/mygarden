import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {StorageService} from './storage.service';

@Injectable({providedIn: 'root'})
export class Guard implements CanActivate {
  constructor(private storageService: StorageService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.storageService.isFirstLaunch().then((firsLaunch: boolean) => {
      if (!firsLaunch) {
        console.log('First launch: ', firsLaunch, ' Guard: unlock');
        return true;
      } else {
        console.log('First launch: ', firsLaunch, ' guard: lock');
        this.router.navigate(['welcome-screen']);
        return false;
      }
    });
  }
}
