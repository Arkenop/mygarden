import {Component, OnInit} from '@angular/core';
import {StatusBar, Style} from '@capacitor/status-bar';
import {isPlatform} from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor() {
  }

  ngOnInit() {
    if (isPlatform('mobile')) {
      // Set status bar color
      const setStatusBarStyle = async () => {
        await StatusBar.setStyle({style: Style.Light});
        await StatusBar.setBackgroundColor({color: '#ffffff'});
      };
      setStatusBarStyle().then();
    }
  }
}
