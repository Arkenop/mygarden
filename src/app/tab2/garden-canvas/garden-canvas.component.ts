import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FabricService} from './fabric.service';
import {AlertController} from '@ionic/angular';
import {StorageService} from '../../storage.service';

@Component({
  selector: 'app-garden-edit-draw',
  templateUrl: 'garden-canvas.component.html',
  styleUrls: ['garden-canvas.component.scss']
})
export class GardenCanvasComponent implements OnInit, OnChanges {
  fabric: any;
  isLocked = true;
  currentYear = new Date().getFullYear();
  @Input() gardenName: string;
  @Input() year: number;

  constructor(
    private fabricService: FabricService,
    public alertController: AlertController,
    private storageService: StorageService) {
  }

  ngOnInit() {
    this.fabric = this.fabricService.initFabric(document);
    this.fabricService.restoreFromJSON(this.gardenName, this.year);
    this.storageService.gardenSelected.subscribe((garden: string) => {
      this.fabricService.restoreFromJSON(garden, this.year);
    });
  }

  async ngOnChanges(changes: SimpleChanges) {
    if ('year' in changes && !this.isLocked) {
      const alert = await this.alertController.create({
        header: 'Changement d\'année',
        message: 'Voulez-vous sauvegarder les modifications ?',
        buttons: [
          {
            text: 'Non',
            role: 'cancel',
            cssClass: 'secondary',
            id: 'cancel-button',
            handler: () => {
              this.fabricService.restoreFromJSON(this.gardenName, this.currentYear);
              this.isLocked = true;
            }
          }, {
            text: 'Oui',
            id: 'confirm-button',
            handler: () => {
              this.onEdit();
            }
          }
        ],
      });
      await alert.present();
    }
  }

  onEdit() {
    this.isLocked = !this.isLocked;
    if (this.isLocked) {
      this.fabricService.lockEdit();
      this.fabricService.saveGardenDraw(this.gardenName, this.currentYear);
    } else {
      this.fabricService.unlockEdit();
    }
  }

  onAddRect() {
    this.fabricService.addRect();
  }

  onAddCircle() {
    this.fabricService.addCircle();
  }

  onAddTriangle() {
    this.fabricService.addTriangle();
  }

  async onLoadVegetables() {
    const alert = await this.alertController.create({
      header: 'Confirmation',
      message: 'Êtes-vous vraiment sûr de vouloir générer un jardin ?',
      buttons: ['Annuler', 'Continuer']
    });

    await alert.present();
  }

  onAddText() {
    this.fabricService.addText();
  }
}
