import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {StorageService} from '../../storage.service';
import {Garden} from '../../shared/garden.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {VegetablesService} from '../../shared/vegetables.service';

export interface VegetableGardenModel {
  name: string;
  isChecked: boolean;
}

@Component({
  selector: 'app-garden-edit',
  templateUrl: './garden-edit.component.html',
  styleUrls: ['./garden-edit.component.scss'],
})
export class GardenEditComponent implements OnInit {
  gardenForm: FormGroup;
  forbiddenGardens = [];
  formIsReady = false;
  allVegetablesAreSelected = false;
  vegetablesForm: VegetableGardenModel[];
  garden: Garden; // Garden data if in edit mode
  @Input() editMode = false;
  @Input() id: string; // If the editMode is true, it contains the name of the editing garden

  constructor(private modalCtrl: ModalController,
              private storageService: StorageService,
              private vegetableService: VegetablesService) {
  }

  async ngOnInit() {
    // Get gardens names and add them to the blacklist
    this.storageService.getGardens().then((gardens: Garden[]) => {
      this.forbiddenGardens = gardens === null ? [] : gardens;
      this.forbiddenGardens.push({name: 'firstTime'});
    });
    await this.storageService.getGarden(this.id).then((garden: Garden) => {
      this.garden = garden;
      // Get all vegetable selected and prepare data to show them in the form
      this.vegetablesForm = this.vegetableService.getVegetables().map((vegetable) => {
        return {name: vegetable.name, isChecked: this.editMode && this.garden.vegetable.includes(vegetable.name)};
      });
      this.formInit();
    });
  }

  forbiddenNames(control: FormControl): { [s: string]: boolean } {
    // Check if the name is correct or not
    if (this.forbiddenGardens !== undefined && this.forbiddenGardens !== null) {
      if (this.forbiddenGardens.map((gardenToCheck) => {
        return gardenToCheck.name;
      }).indexOf(control.value) !== -1) {// Convert Object to array
        return {['nameIsForbidden']: true}; // Name is forbidden
      } else {
        return null; // Name is allow
      }
    }
    return null;
  }

  onSelectAll() {
    this.allVegetablesAreSelected = !this.allVegetablesAreSelected;
    this.vegetablesForm.map((vegetable: VegetableGardenModel) => {
      return this.allVegetablesAreSelected ? vegetable.isChecked = true : vegetable.isChecked = false;
    });
  }

  /**
   * Save garden into local storage
   */
  onSave() {
    // Create list for selected vegetables
    this.gardenForm.value.vegetable = [];
    this.vegetablesForm.forEach((vegetable: VegetableGardenModel) => {
      if (vegetable.isChecked === true) {
        this.gardenForm.value.vegetable.push(
          vegetable.name
        );
      }
    });
    console.log('Values going to be saved :', this.gardenForm.value);
    if (this.gardenForm.valid) {
      if (this.editMode) {
        this.storageService.updateGarden(this.id, this.gardenForm.value);
      } else {
        this.storageService.addGarden(this.gardenForm.value);
      }
    } else {
      console.warn('vegetableForm invalid ! \n', this.gardenForm);
    }
    this.editMode = false;
    this.dismissModal();
  }

  dismissModal() {
    this.editMode = false;
    this.modalCtrl.dismiss();
  }

  private formInit() {
    this.formIsReady = false;
    let name = '';
    let description = '';
    // Vegetables uses NgModel

    if (this.editMode) {
      // Update form from garden data
      name = this.garden.name;
      description = this.garden.description;

      if (this.vegetableService.getVegetablesLen() === this.garden.vegetable.length) {
        this.allVegetablesAreSelected = true;
      }
    }
    this.formIsReady = true;

    // Init the vegetable Form
    this.gardenForm = new FormGroup({
      name: new FormControl(
        name,
        [Validators.required,
          this.forbiddenNames.bind(this),
          Validators.maxLength(20),
          Validators.minLength(3)]),
      description: new FormControl(description),
    });
  }
}
